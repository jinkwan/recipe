# -*- coding: utf-8 -*-

import sys
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib


class Node:
    def __init__(self, size):
        self.size = size

class Edge:
    def __init__(self, pair, weight):
        self.pair = pair
        self.weight = weight

class NodeLabel:
    def __init__(self, position, name, size):
        self.position = position
        self.name = name
        self.size = size

class EdgeLabel:
    def __init__(self, position, name, size):
        self.position = position
        self.name = name
        self.size = size


def MoveLabel(position, xoff, yoff):
    """
    :rtype : dict
    """
    labels = {}
    for i in position:
        x, y = position[i]
        labels[i] = x+xoff, y+yoff
    return labels


reload(sys)
sys.setdefaultencoding('utf-8')
matplotlib.rc('font', family='HYsanB')  # 한글 폰트 설정

EDGE_THRESHOLD = 3

fin = open("small verb.txt", "r")
G = nx.Graph()

# verb loading
verbLine = fin.readline()
verbList = verbLine.split(',')
verb = []
for i in verbList:
    verb.append([i])
numVerb = len(verb)

# menu loading
menu = []
while True:
    line = fin.readline()
    if line == '':
        break
    cat = line.split(',')
    menu.append(cat)
numMenu = len(menu)

print "메뉴 수 : ", numMenu
print "동사 수 : ", numVerb

# init adjMatrix
adjMat = []
for i in range(numMenu):
    adjMat.append([])
    for j in range(numMenu):
        adjMat[i].append(0)

# 음식 사이의 연관정보 정리
for i in range(numMenu-1):
    for j in range(i+1, numMenu):
        for k in range(numVerb):
            if menu[i][k] != '' and menu[i][k] == menu[j][k]:
                adjMat[i][j] += 1

degrees = []
for i in G.nodes():
    degrees.append([G.degree(i), i])
degrees.sort()

# adding nodes
for i in range(numMenu):
    G.add_node(i)

# adding edges
# TODO 에지 연결할 때 웨이트 맞춰서 굵기 조절
# TODO 에지 연결할 때 엔트로피 계산 결과를 이용하도록
edgeList = []
for i in range(numMenu-1):
    for j in range(i, numMenu):
        if adjMat[i][j] > EDGE_THRESHOLD:
            G.add_edge(i, j)
            edgePair = (i, j)
            edgeWeight = adjMat[i][j]
            edge = Edge(edgePair, edgeWeight)
            edgeList.append(edge)

T = nx.minimum_spanning_tree(G)

# font, node size setting
fontSize = []
nodeSize = []
for i in T.nodes():
    nodeSize.append(pow(T.degree(i)/10.0, 2)*300)
    fontSize.append(T.degree(i)*10)

# graph drawing
# pos = nx.circular_layout(G)  # set layout as circular
pos = nx.fruchterman_reingold_layout(T)  # set layout as fruchterman_reingold
# pos = nx.random_layout(G)  # set layout as random
# pos = nx.shell_layout(G)  # set layout as shell
# pos = nx.spectral_layout(G)  # set layout as spectral
# pos = nx.spring_layout(G, k=0.8)  # set layout as spring

# label 위치를 변경하는 방법
# 원래 position 을 받아서 position 에 사용할 위치 값을 조금 조정해서 이 값으로 label 달면
labelPos = MoveLabel(pos, 0, 0.05)

"""
degree 상위 5개의 노드에만 레이블을 달도록
1. degree 파악
2. degree 정렬
3. 5회까지 보면서 레이블 추가
"""
degrees = []
for i in T.nodes():
    degrees.append([T.degree(i), i])
degrees.sort()

nodeLabel = []
for i in T.nodes():
    posdict = {i: pos[i]}
    namedict = {i: menu[i][0]}
    labelSize = T.degree(i)
    label = NodeLabel(posdict, namedict, labelSize)
    nodeLabel.append(label)

fig = plt.figure(figsize=(20, 20), dpi=100)

# plt.savefig("path_graph1.png")
nx.draw(T)
plt.show()

fin.close()
